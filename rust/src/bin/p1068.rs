/*
    Weird Algorithm 
    https://cses.fi/problemset/task/1068
*/

use std::io::{BufRead, BufReader};
 
fn main() {
    let mut input = BufReader::new(std::io::stdin());
    let mut line = "".to_string();
    input.read_line(&mut line).unwrap();
    let mut split = line.split_whitespace();
    let mut a: usize = split.next().unwrap().parse().unwrap();
 
    while a != 1 {
        print!("{} ", a);
        
        if a % 2 == 0 {
            a = a / 2 ;
        } else {
            a = (a * 3) + 1;
        }
    }
    println!("{}", a);
}