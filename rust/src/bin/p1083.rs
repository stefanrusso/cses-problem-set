/*
    Missing Number
    https://cses.fi/problemset/task/1083
*/

use std::io::{BufRead, BufReader};
 
fn main() {
    let mut input = BufReader::new(std::io::stdin());
    let mut line1 = String::new();
    input.read_line(&mut line1).unwrap();
    let mut split = line1.split_whitespace();
    let num: usize = split.next().unwrap().parse().unwrap();
    let num_sum: usize = (num * (num + 1)) / 2;
 
    let mut line2 = String::new();
    input.read_line(&mut line2).unwrap();
    let nums : Vec<usize> = line2.split_whitespace().map(|s| s.parse().expect("Parse error")).collect();
    let nums_sum: usize = nums.iter().sum();
    println!("{}", num_sum - nums_sum);
}