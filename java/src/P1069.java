import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class P1069 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        String[] chars = {"A", "C", "T", "G"};
        long max = 0;
        for(String c : chars) {
            String regex = "(" + c + "+)";
//            String[] splits = input.split(c);
//            int[] sizes = Arrays.stream(splits).mapToInt(x -> x.length()).toArray();
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(input);

            while(matcher.find()) {
                long matchLength = matcher.group().length();
                if (matchLength > max) {
                    max = matchLength;
                }
            }
        }
        System.out.println(max);
    }
}
