import java.util.*;

public class P1083 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextInt();

        long sum = 0;
        for (int i = 0; i < n - 1; i++) {
            sum += scanner.nextInt();
        }
        System.out.println((n*(n+1)/2 - sum));
    }
}
